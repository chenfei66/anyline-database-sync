
```
 数据库配置说明
 BS_DATASOURCE:配置数据源  注意帐号密码URL等敏感信息最好经过非对称加密,把私钥放在jar中再把jar加密
      CODE                    : 数据源标识,后续将根据这个值切换数据源
      DRIVER                  : 驱动类(根据数据库类型,如:com.mysql.cj.jdbc.Driver)
      URL                     : JDBC连接
      ACCOUNT                 : 帐号
      PASSWORD                : 密码
      TYPE_CLASS              : 连接池类(默认:com.zaxxer.hikari.HikariDataSource)

 SYNC_TASK:同步配置
      TRIGGER_TYPE_CODE       : 触发方式 0:定时 1:被动
      DATA_STATUS             : 状态 0:不可用 不会执行 1:可用
      TYPE_CODE               : 0:全量(先truncate目标表再insert)
                                1:增量
                                2:更新
      INTERVALS               : 执行间隔时间(秒)
      SCHEDULE_ID             : 定时器(如果任务本身就在定时器中不要设置这个值)
      SRC_DATASOURCE_CODE     : 源数据源(BS_DATASOURCE.CODE)
      SRC_TABLE               : 源表
                                如果有多个表以,分隔,同时保持与TAR_TABLE中的表数量一致
                                如果要同步整个数据库可以SRC_TABLE设置成(TAR_TABLE值将被忽略)
                                *表示所有表
                                *-T1,T2 表示同步除了T1,T2的所有表

      TAR_DATASOURCE_CODE     : 目标数据源
      TAR_TABLE               : 目标表(SRC_TABLE中出现时 忽略当前值)
      COLS                    : 源表与目标表的 列对应关系(源列名:目标列名)
                                如ID:CD,NAME:NM,如果有一样列可以ID,NAME:NM,表示全部
                                如果SRC_TABLE中设置了多个值或,忽略当前值
      ORDER_COLUMN            : 排序列
      PK                      : 在更新模式下用到,根据主键更新目标表(单表时有效，多表时自动检测)


      AFTER_SQL_SRC           : 执行成功后在源数据库中执行的SQL,支持变量 
                                ${table}:当前表 
                                ${order}:排序列 
                                ${max}:当前同步的排序列最大值 
      AFTER_SQL_TAR           : 执行成功后在目标数据库中执行的SQL

      STEP                    : 每次同步多少行
      LAST_EXE_TIME           : 最后执行时间(注意多表的情况)
      LAST_EXE_VALUE          : 最后同步的主键值(注意多表的情况)
      LAST_EXE_QTY            : 最后一次同步了多少行(注意多表的情况)
      EXE_QTY                 : 一共同步了多行行(注意多表的情况)


SQL脚本:
drop table if exists BS_DATASOURCE;
create table BS_DATASOURCE
(
   ID                   bigint not null auto_increment comment '主键 ',
   CODE                 varchar(50) comment 'CODE ',
   TITLE                varchar(50) comment '标题 ',
   TYPE_CLASS           varchar(50) comment '类型 ',
   DRIVER               varchar(200) comment '驱动 ',
   URL                  varchar(200) comment 'URL ',
   ACCOUNT              varchar(20) comment '帐号 ',
   PASSWORD             varchar(32) comment '密码 ',
   IDX                  varchar(10) comment '排序 ',
   REMARK               varchar(500) comment '备注 ',
   REG_ID               varchar(50) comment '注册人 ',
   REG_IP               varchar(20) comment '注册IP ',
   REG_TIME             datetime default CURRENT_TIMESTAMP comment '注册时间 ',
   REG_CLIENT_ID        varchar(50) comment '注冊端ID ',
   UPT_ID               varchar(50) comment '修改人 ',
   UPT_IP               varchar(20) comment '修改人IP ',
   UPT_TIME             datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间 ',
   UPT_CLIENT_ID        varchar(50) comment '修改端ID ',
   DATA_STATUS          int default 1 comment '活动状态 ',
   TENANT_CODE          varchar(50) default '0' comment '租户CODE ',
   ORG_CODE             varchar(50) comment '组织CODE ',
   DATA_VERSION         varchar(50) default '0' comment '数据版本 ',
   primary key (ID)
)comment = 'BS_数据源';

drop table if exists SYNC_TASK; 
create table SYNC_TASK
(
   ID                   bigint not null auto_increment comment '主键 ',
   CODE                 varchar(50) comment 'CODE ',
   SRC_DATASOURCE_CODE  varchar(50) comment '源数据源CODE ',
   TAR_DATASOURCE_CODE  varchar(50) comment '目标数据源CODE ',
   SRC_TABLE            varchar(50) comment '源表 ',
   TAR_TABLE            varchar(50) comment '目标表 ',
   COLS                 varchar(2000) comment '同步列 源列名:目标列名,',
   TYPE_CODE            int comment '同步方式CODE 0:全量,1:增量,2:更新',
   LAST_EXE_TIME        datetime comment '最后同步时间 ',
   LAST_EXE_VALUE       varchar(50) comment '最后同步值 ',
   LAST_EXE_QTY         bigint comment '最后执行数量 ',
   PK                   varchar(50) comment '更新依据主键(目标表) ',
   ORDER_COLUMN         varchar(50) comment '排序列 ',
   STEP                 int comment '步长 ',
   TRIGGER_TYPE_CODE    int comment '触发方式CODE 0定时 1.被动',
   SCHEDULE_ID          bigint comment '定时器ID ',
   INTERVALS            int comment '执行间隔(s) ',
   AFTER_SQL_SRC        text comment '执行后SQL_SRC ',
   AFTER_SQL_TAR        text comment '执行后SQL_TAR ',
   AFTER_METHOD         varchar(100) comment '执行后方法 ',
   EXE_QTY              int comment '执行数量 ',
   EXE_EXCEPTION        text comment '执行异常 ',
   IDX                  varchar(10) comment '排序 ',
   REMARK               varchar(500) comment '备注 ',
   REG_ID               varchar(50) comment '注册人 ',
   REG_IP               varchar(20) comment '注册IP ',
   REG_TIME             datetime default CURRENT_TIMESTAMP comment '注册时间 ',
   REG_CLIENT_ID        varchar(50) comment '注冊端ID ',
   UPT_ID               varchar(50) comment '修改人 ',
   UPT_IP               varchar(20) comment '修改人IP ',
   UPT_TIME             datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间 ',
   UPT_CLIENT_ID        varchar(50) comment '修改端ID ',
   DATA_STATUS          int default 1 comment '活动状态 ',
   TENANT_CODE          varchar(50) default '0' comment '租户CODE ',
   ORG_CODE             varchar(50) comment '组织CODE ',
   DATA_VERSION         varchar(50) default '0' comment '数据版本 ',
   primary key (ID)
)comment = 'SYNC_同步任务';

```