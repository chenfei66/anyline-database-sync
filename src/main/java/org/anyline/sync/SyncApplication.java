package org.anyline.sync;

import org.anyline.data.jdbc.ds.DynamicDataSourceRegister;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;


@ComponentScan(basePackages = {"org.anyline"})
@SpringBootApplication
@EnableScheduling
@Import(DynamicDataSourceRegister.class)
public class SyncApplication {
    public static void main(String[] args) throws Exception{
        SpringApplication application = new SpringApplication(SyncApplication.class);
        application.run(args);
    }
}
